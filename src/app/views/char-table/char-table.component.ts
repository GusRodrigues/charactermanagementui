import { Component, OnInit, ViewChild } from '@angular/core';
import { Character } from 'src/app/models/character';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

const CHARACTERS: Character[] = [
  {"name": "Duyn","gender": false,"playable": true,"experiencePoint": 0,
    "attributes": {"strength": 14,"dexterity": 8,"constitution": 15,
      "intelligence": 10,"wisdom": 16,"charisma": 12
    }},
  {"name": "Nuyn", "gender": false, "playable": false, "experiencePoint": 0,
    "attributes": {"strength": 14, "dexterity": 8, "constitution": 15,
      "intelligence": 10, "wisdom": 16, "charisma": 12
    }},
];

@Component({
  selector: 'app-char-table',
  templateUrl: './char-table.component.html',
  styleUrls: ['./char-table.component.css']
})

export class CharTableComponent implements OnInit {

  displayedColumns: string[] = ['name','playable', 'gender','attributes'];
  dataSource = new MatTableDataSource<Character>(CHARACTERS);

  constructor() { }
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  ngOnInit() : void {
    this.dataSource.paginator = this.paginator;
  }

}
