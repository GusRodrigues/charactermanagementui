import { Id } from './id';

/**
 * Representation of a Character
 */
export interface Size extends Id {
    name: string;
    hitDice: number;
}
