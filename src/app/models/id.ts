/**
 * Representation of an ID for all entities of the game
 */
export interface Id {
    id: number | null;
}
