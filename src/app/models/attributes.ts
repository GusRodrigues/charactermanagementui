/**
 * All attributes of a character. It is embededed into the Character.
 */
export interface Attributes {
    strength: number;
    dexterity: number;
    constitution: number;
    intelligence: number;
    wisdom: number;
    charisma: number;
}
