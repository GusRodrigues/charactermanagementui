import { Attributes } from './attributes';
import { Id } from './id';

/**
 * Representation of a Character
 */
export interface Character extends Id {
    name: string;
    gender: boolean;
    playable: boolean;
    experiencePoint: number;
    attributes: Attributes;
}
