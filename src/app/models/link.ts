/**
 * Representation of a HATEOAS Link. It must ALWAYS contains an href link called... href
 */
export interface Link {
    href: string;
}
