import { Id } from './id';

/**
 * Representation of a class in the game. We refer as JOB due to reserved word in TS and Java
 */
export interface Job extends Id {

    name: string;
}
