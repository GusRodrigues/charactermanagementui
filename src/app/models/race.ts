import { Id } from './id';

/**
 * Representation of a Character
 */
export interface Race extends Id {
    race: string;
}
