import { Id } from './id';

/**
 * Representation of an Alignment
 */
export interface Alignment extends Id {
    /**
     * Lawful alignment may be:
     * true - lawful
     * false - chaotic
     * null - neutral
     */
    lawful: boolean | null;
    /**
     * Gppd alignment may be:
     * true - good
     * false - evil
     * null - neutral
     */
    good: boolean | null;
    /**
     * The name of the alignment
     */
    name: string;
}
